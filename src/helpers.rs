use chrono::{DateTime, Utc};
use html_entities::decode_html_entities;
use rocket_contrib::templates::handlebars::{
    html_escape, Context, Handlebars, Helper, HelperResult, Output, RenderContext,
};
use serde_json;
use std::collections::VecDeque;
use thousands::Separable;

fn is_nsfw(context: &Context, url: &str) -> bool {
    let nsfw = context
        .navigate(".", &VecDeque::new(), "nsfw")
        .expect("Invalid NSFW list")
        .expect("No NSFW list");
    nsfw.get(url).map_or(false, |b| {
        serde_json::from_value::<bool>(b.clone()).expect("non-boolean nsfw")
    })
}

fn render_url(context: &Context, url: &str, title: &str) -> String {
    let title = decode_html_entities(title).unwrap_or_else(|_| title.to_string());
    let cls = if is_nsfw(context, url) {
        " class=\"nsfw\""
    } else {
        ""
    };
    format!(
        "<a{} href=\"{}\" rel=\"noopener noreferrer\">{}</a>",
        cls,
        html_escape(url),
        html_escape(&title)
    )
}

pub fn inc(
    helper: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut Output,
) -> HelperResult {
    let param = helper
        .param(0)
        .expect("Param 0 is required for inc helper.");
    let num = serde_json::from_value::<u64>(param.value().clone())
        .expect("Param 0 to inc helper was not a number.");
    let rendered = format!("{}", num + 1_u64);
    out.write(&rendered)?;
    Ok(())
}

pub fn num(
    helper: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut Output,
) -> HelperResult {
    let param = helper
        .param(0)
        .expect("Param 0 is required for num helper.");
    let num = serde_json::from_value::<u64>(param.value().clone())
        .expect("Param 0 to num helper was not a number.");
    let rendered = num.separate_with_spaces();
    out.write(&rendered)?;
    Ok(())
}

pub fn comma(
    helper: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut Output,
) -> HelperResult {
    let param = helper
        .param(0)
        .expect("Param 0 is required for comma helper.");
    let num = serde_json::from_value::<u64>(param.value().clone())
        .expect("Param 0 to comma helper was not a number.");
    if num > 0 {
        out.write(",")?;
    }
    Ok(())
}

pub fn link(
    helper: &Helper,
    _: &Handlebars,
    context: &Context,
    _: &mut RenderContext,
    out: &mut Output,
) -> HelperResult {
    let param = helper
        .param(0)
        .expect("Param 0 is required for link helper.");
    let link = serde_json::from_value::<String>(param.value().clone())
        .expect("Param 0 to link helper was not a string.");

    let rendered = render_url(context, &link, &link);
    out.write(&rendered)?;
    Ok(())
}

pub fn video(
    helper: &Helper,
    _: &Handlebars,
    context: &Context,
    _: &mut RenderContext,
    out: &mut Output,
) -> HelperResult {
    let param = helper
        .param(0)
        .expect("Param 0 is required for video helper.");
    let video = serde_json::from_value::<String>(param.value().clone())
        .expect("Param 0 to video helper was not a string.");

    let mut lines = video.lines();
    let title = lines.next().expect("No video title").to_string();
    let url = lines.next().expect("No video url").to_string();
    if lines.next().is_some() {
        panic!("Extra lines in video key");
    }

    let rendered = render_url(context, &url, &title);
    out.write(&rendered)?;
    Ok(())
}

pub fn datetime(
    helper: &Helper,
    _: &Handlebars,
    _: &Context,
    _: &mut RenderContext,
    out: &mut Output,
) -> HelperResult {
    let param = helper
        .param(0)
        .expect("Param 0 is required for datetime helper.");
    let datetime = serde_json::from_value::<DateTime<Utc>>(param.value().clone())
        .expect("Param 0 to datetime helper was not a DateTime<UTC>.");
    let rendered = format!(
        "{}",
        datetime.timestamp() * 1000 + i64::from(datetime.timestamp_subsec_millis())
    );
    out.write(&rendered)?;
    Ok(())
}
