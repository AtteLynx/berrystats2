use crate::counter::Counter;
use berrylogs::Line;
use chrono::{DateTime, Duration, Timelike, Utc};
use std::collections::HashMap;

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Serialize)]
pub struct User {
    lines: usize,
    chars: usize,
    emotes: usize,
}

#[derive(Clone, Debug, Default, Serialize)]
pub struct Stats {
    lines: usize,
    connected: Vec<(DateTime<Utc>, isize)>,
    users: Counter<User>,
    emotes: Counter<usize>,
    drinks: Counter<usize>,
    links: Counter<usize>,
    videos: Counter<usize>,
    nsfw: HashMap<String, bool>,
}

impl Stats {
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }

    // TODO: optimize
    pub fn add_connected(&mut self, time: &DateTime<Utc>, delta: isize) {
        if let Some(tail) = self.connected.pop() {
            if tail.0.signed_duration_since(*time) > Duration::minutes(10)
                || time.minute() % 10 != tail.0.minute() % 10
            {
                self.connected.push(tail);
            } else {
                let num = tail.1 as isize + delta;
                #[allow(clippy::cast_sign_loss)]
                self.connected.push((*time, if num < 0 { 0 } else { num }));
            }
        }
    }

    pub fn add_line(&mut self, line: &Line) {
        self.lines += 1;

        // topics, joins/parts
        if line.nick == "--" {
            if let Ok(nicks) = line.nick_list() {
                #[allow(clippy::cast_possible_wrap)]
                self.connected.push((line.time, nicks.count() as isize));
            } else if let Ok(video) = line.video_change() {
                if video.title != "~ Raw Livestream ~" {
                    let key = format!("{}\n{}", video.title, video.url);
                    self.videos.incr(&key);

                    if line.message.to_lowercase().contains("nsfw") {
                        self.nsfw.insert(video.url, true);
                    }
                }
            }
            return;
        } else if line.nick == "<--" {
            self.add_connected(&line.time, -1);
            return;
        } else if line.nick == "-->" {
            self.add_connected(&line.time, 1);
            return;
        } else if line.nick == "*" {
            // TODO: requests
            return;
        }

        self.users.incr_with(&line.nick, |user| {
            user.lines += 1;
            user.chars += line.message.len();
        });

        // drinks
        if line.mode.map_or(false, |m| m != '+') && line.message.ends_with(" drink!") {
            self.drinks.incr(&line.message[..line.message.len() - 7]);
        }

        // emotes
        for emote in line.emotes() {
            self.emotes.incr(emote.name);
            self.users.incr_with(&line.nick, |user| {
                user.emotes += 1;
            });
        }

        // links
        for link in line.links() {
            if line.message.to_lowercase().contains("nsfw") {
                self.nsfw.insert(link.to_string(), true);
            }
            self.links.incr(link);
        }
    }
}
