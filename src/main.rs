#![feature(proc_macro_hygiene, decl_macro, never_type)]
#![deny(clippy::all, clippy::pedantic)]
#![allow(clippy::missing_docs_in_private_items)]

// TODO: remove once macro imports work nicely
#[macro_use]
extern crate serde_derive;

use rocket::{
    get,
    http::ContentType,
    outcome::Outcome,
    request::{self, FromRequest},
    response::Content,
    routes, Request,
};
use rocket_contrib::{json::Json, serve::StaticFiles, templates::Template};
use std::collections::HashSet;

mod counter;
mod helpers;
mod stats;
mod views;
use crate::stats::Stats;

#[derive(Clone, Copy)]
struct RequestedTimespan {
    pub days: usize,
}

impl FromRequest<'_, '_> for RequestedTimespan {
    type Error = !;

    fn from_request(request: &Request) -> request::Outcome<Self, Self::Error> {
        Outcome::Success(Self {
            days: request
                .uri()
                .query()
                .and_then(|q| q.parse().ok())
                .unwrap_or(7),
        })
    }
}

#[get("/")]
fn index(timespan: RequestedTimespan) -> Template {
    Template::render("index", views::get_stats(timespan.days))
}

#[get("/stats.json")]
fn stats_json(timespan: RequestedTimespan) -> Json<Stats> {
    Json(views::get_stats(timespan.days))
}

#[get("/nicks.json")]
fn nicks_json(timespan: RequestedTimespan) -> Json<HashSet<String>> {
    Json(views::get_nicks(timespan.days))
}

#[get("/ping")]
fn ping() -> Content<&'static str> {
    Content(ContentType::Plain, "pong")
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, stats_json, nicks_json, ping])
        .mount("/static", StaticFiles::from("static"))
        .attach(Template::custom(|engines| {
            engines
                .handlebars
                .register_helper("inc", Box::new(helpers::inc));
            engines
                .handlebars
                .register_helper("num", Box::new(helpers::num));
            engines
                .handlebars
                .register_helper("comma", Box::new(helpers::comma));
            engines
                .handlebars
                .register_helper("link", Box::new(helpers::link));
            engines
                .handlebars
                .register_helper("video", Box::new(helpers::video));
            engines
                .handlebars
                .register_helper("datetime", Box::new(helpers::datetime));
        }))
        .launch();
}
