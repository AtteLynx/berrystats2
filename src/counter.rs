use itertools::Itertools;
use linked_hash_map::LinkedHashMap;
use serde::{Serialize, Serializer};
use std::ops::AddAssign;

pub trait Countable: Clone + Default + Ord + Serialize {}
impl<T: Clone + Default + Ord + Serialize> Countable for T {}

#[derive(Clone, Debug, Default, Eq, PartialEq, PartialOrd, Ord)]
pub struct Counter<T>
where
    T: Countable,
{
    entries: LinkedHashMap<String, T>,
}

impl<T> Counter<T>
where
    T: Countable,
{
    pub fn incr_with<F>(&mut self, key: &str, fun: F)
    where
        F: FnMut(&mut T),
    {
        if !self.entries.contains_key(key) {
            self.entries.insert(key.to_string(), T::default());
        }

        self.entries.get_refresh(key).map(fun);
    }

    pub fn top(&self, num: usize) -> Vec<(&String, &T)> {
        self.entries
            .iter()
            .sorted_by(|a, b| Ord::cmp(b.1, a.1))
            .take(num)
            .collect()
    }
}

impl<T> Counter<T>
where
    T: Countable + AddAssign<usize>,
{
    #[inline]
    pub fn incr(&mut self, key: &str) {
        self.incr_with(key, |v| {
            *v += 1;
        });
    }
}

impl<T> Serialize for Counter<T>
where
    T: Countable,
{
    #[inline]
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        // TODO: make number configurable
        serializer.collect_seq(self.top(10))
    }
}
