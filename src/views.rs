use crate::stats::Stats;
use chrono::{Duration, Utc};
use std::collections::HashSet;

pub fn get_stats(days: usize) -> Stats {
    let cutoff = Utc::now() - Duration::days(days as i64);
    let mut stats = Stats::new();
    for line in berrylogs::lines_n(days + 1).skip_while(|line| line.time < cutoff) {
        stats.add_line(&line);
    }
    stats
}

pub fn get_nicks(days: usize) -> HashSet<String> {
    let cutoff = Utc::now() - Duration::days(days as i64);
    let mut nicks: HashSet<String> = berrylogs::lines_n(days + 1)
        .skip_while(|line| line.time < cutoff)
        .map(|line| line.nick)
        .collect();
    nicks.remove("--");
    nicks.remove("<--");
    nicks.remove("-->");
    nicks.remove("*");
    nicks
}
