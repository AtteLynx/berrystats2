'use strict';

setTimeout(() => {
    const chart = Highcharts.chart('chart', {
        chart: {
            animation: false
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        legend: {
            enabled: false
        },
        time: {
            useUTC: false
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 3
                },
                turboThreshold: 1
            }
        },
        series: [
            {
                name: 'Users',
                color: '#7cb5ec',
                data: JSON.parse(document.getElementById('chart-data').text)
            }
        ],
        tooltip: {
            valueDecimals: 0
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: 'Local time'
            }
        },
        yAxis: {
            title: {
                text: 'Users'
            },
            min: 0,
            maxPadding: 0
        }
    });
    setTimeout(() => { chart.reflow(); }, 100);
}, 0);

// emote tooltips
setTimeout(() => {
    const emoteImages = {};
    for (const el of document.querySelectorAll('[data-emote]')) {
        const img = new Image();
        img.id = 'emote-tip';
        img.src = 'https://atte.fi/berrymotes/render.php?emote=' + el.dataset.emote;
        emoteImages[el.dataset.emote] = img;
    }

    const emoteTable = document.getElementById('emotes');
    emoteTable.addEventListener('mouseenter', event => {
        if (event.target.dataset.emote) {
            event.stopPropagation();
            document.getElementById('emote-tip').replaceWith(emoteImages[event.target.dataset.emote]);
            document.body.classList.add('emote-tip-shown');
        }
    }, true);
    emoteTable.addEventListener('mouseleave', event => {
        document.body.classList.remove('emote-tip-shown');
    });
    emoteTable.addEventListener('mousemove', event => {
        document.getElementById('emote-tip').style.transform = `translate(${event.pageX}px, ${event.pageY}px)`;
    });
}, 1000);
